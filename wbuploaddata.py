import os
import wandb
from pathlib import Path

os.system('wandb login 187758f8acd4c065f5f10a982e516222c1e743b1')

run = wandb.init(project="gptfimodel", job_type="upload")

for x in range(8):

    strx = str(x)

    artifact = wandb.Artifact("gptfimodeldata_"+strx, type="raw_data")

    # paths = [str(x)
    #          for x in Path("data/").glob("**/*.txt")]
    paths = ["data/train"+strx+".txt","data/eval"+strx+".txt"]

    print(paths)

    for path in paths:

        artifact.add_file(path)

    run.log_artifact(artifact)