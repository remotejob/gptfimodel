import os
import wandb
from pathlib import Path

os.system('wandb login 187758f8acd4c065f5f10a982e516222c1e743b1')

run = wandb.init(project="gptfimodel", job_type="upload")

artifact = wandb.Artifact("gptfimodeltokenazer", type="raw_data")

paths = [str(x)
         for x in Path("data/tokenizer").glob("**/*")]
# paths = ["data/train"+strx+".txt","data/eval"+strx+".txt"]

print(paths)

for path in paths:

    artifact.add_file(path)

run.log_artifact(artifact)