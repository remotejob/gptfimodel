import os
import sys
import pandas as pd
from tqdm import tqdm
from pathlib import Path
from tokenizers.models import BPE
from tokenizers import Tokenizer
from tokenizers.decoders import ByteLevel as ByteLevelDecoder
from tokenizers.normalizers import NFKC, Sequence
from tokenizers.pre_tokenizers import ByteLevel
from tokenizers.trainers import BpeTrainer
import wandb

os.system('pip install --upgrade git+https://github.com/intake/filesystem_spec')
os.system('pip install --upgrade datasets')

from datasets import load_dataset

from tokenizers import ByteLevelBPETokenizer
from tokenizers.processors import BertProcessing

start = False
epochs = 33
runnum = "9"
datafile = "1"
acc = "almazurov"

# os.environ["WANDB_DISABLED"] = "true"
os.system('wandb login 187758f8acd4c065f5f10a982e516222c1e743b1')

run = wandb.init(project="gptfimodel", entity='remotejob2', config={"epochs": epochs, "acc":acc,"runnum":runnum})

artifact = run.use_artifact('remotejob2/gptfimodel/gptfimodeldata_'+datafile+':v0', type='raw_data')
artifact_dir = artifact.download()

train_path = artifact_dir + '/' + 'train'+datafile+'.txt'
eval_path = artifact_dir + '/' + 'eval'+datafile+'.txt'

artifact = run.use_artifact('remotejob2/gptfimodel/gptfimodeltokenazer:v2', type='raw_data')
artifact_dir = artifact.download()

tokenizer_path = artifact_dir

if start:
    model_name_or_path = None

else:
    artifact = run.use_artifact('remotejob2/gptfimodel/tweetGPTfimodel:v'+runnum, type='model')
    artifact_dir = artifact.download() 
    model_name_or_path = artifact_dir


wandb.config.update({"epochs": epochs, "acc":acc,"runnum":runnum,"train_path":train_path},allow_val_change=True)


# os.environ["CUDA_LAUNCH_BLOCKING"]='1'  #Makes for easier debugging (just in case)
os.mkdir('./content')
os.mkdir('./content/models')
os.mkdir('./content/models/tweetGPTfi')
weights_dir = "./content/models/tweetGPTfi/weights"
os.mkdir(weights_dir)

if start:

    print("START")

    cmd = '''python ../input/gptfimodeldata/src/run_clm.py --output_dir {0}  \
    --model_type gpt2 \
    --train_file {1} \
    --validation_file {2} \
    --tokenizer_name {4} \
    --do_train \
    --overwrite_output_dir \
    --do_eval \
    --block_size 256 \
    --learning_rate 1e-4 \
    --num_train_epochs={3} \
    --save_total_limit 1 \
    --save_steps 5000 \
    --logging_steps 1000 \
    --per_device_eval_batch_size 16 \
    --per_device_train_batch_size 16 \
    --seed 42 \
    '''.format(weights_dir, train_path, eval_path,epochs,tokenizer_path)

else:        
    print("CONTINUE")

    cmd = '''python ../input/gptfimodeldata/src/run_clm.py --output_dir {0}  \
        --model_name_or_path {4} \
        --train_file {1} \
        --validation_file {2} \
        --tokenizer_name {5} \
        --do_train \
        --overwrite_output_dir \
        --do_eval \
        --block_size 256 \
        --learning_rate 1e-4 \
        --num_train_epochs={3} \
        --save_total_limit 1 \
        --save_steps 5000 \
        --logging_steps 1000 \
        --per_device_eval_batch_size 19 \
        --per_device_train_batch_size 19 \
        --seed 42 \
        '''.format(weights_dir, train_path, eval_path,epochs,model_name_or_path,tokenizer_path)    
 

print(cmd)

os.system(cmd)

lastmodeldir ='./content/models/tweetGPTfi/weights'

artifact = wandb.Artifact('tweetGPTfimodel', type='model')

for entry in os.scandir('./content/models/tweetGPTfi/weights'):
    if entry.is_file():       
        print('./'+lastmodeldir+'/'+entry.name)
        artifact.add_file('./'+lastmodeldir+'/'+entry.name)

run.log_artifact(artifact)

from transformers import pipeline

generator = pipeline('text-generation', model='./content/models/tweetGPTfi/weights',tokenizer=tokenizer_path)
# set_seed(42)

result = generator("Mistä pidät ", max_length=30, num_return_sequences=1)
print(result)

result = generator("Varpaalla masturbointi", max_length=50, num_return_sequences=1)
print(result)

# fill_mask = pipeline(
#     "fill-mask",
#     model="./content/models/tweetGPTfi/weights",
#     tokenizer=tokenizer_path
# )

# result = fill_mask("Mistä pidät <mask>")
# print(result)
# result = fill_mask("<mask> sovitaan treffit")
# print(result)
# result = fill_mask("Espoo <mask>")
# print(result)







