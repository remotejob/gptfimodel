import os
from pathlib import Path
from tokenizers.models import BPE
from tokenizers import Tokenizer
from tokenizers.decoders import ByteLevel as ByteLevelDecoder
from tokenizers.normalizers import NFKC, Sequence
from tokenizers.pre_tokenizers import ByteLevel
from tokenizers.trainers import BpeTrainer
from tokenizers import ByteLevelBPETokenizer


os.system('pip install --upgrade tokenizers==0.9.4')

class BPE_token(object):
    def __init__(self):
        self.tokenizer = Tokenizer(BPE())
        self.tokenizer.normalizer = Sequence([
            NFKC()
        ])
        self.tokenizer.pre_tokenizer = ByteLevel()
        self.tokenizer.decoder = ByteLevelDecoder()

    def bpe_train(self, paths):
        trainer = BpeTrainer(vocab_size=50000, show_progress=True, inital_alphabet=ByteLevel.alphabet(), special_tokens=[
            "<s>",
            "<pad>",
            "</s>",
            "<unk>",
            "<mask>"
        ])
        # self.tokenizer.train(trainer, paths)
        self.tokenizer.train(paths,trainer)

    def save_tokenizer(self, location, prefix=None):
        if not os.path.exists(location):
            os.makedirs(location)
        self.tokenizer.model.save(location, prefix)


paths = [str(x) for x in Path('../input/bertfimodeldata').glob("**/train*.txt")]


# tokenizer = BPE_token()
# # train the tokenizer model
# tokenizer.bpe_train(paths)
# save_path = 'tokenized_data'
# tokenizer.save_tokenizer(save_path)


tokenizer = ByteLevelBPETokenizer()

vocab_size=50000
# Customize training
tokenizer.train(files=paths, vocab_size=vocab_size, min_frequency=5, special_tokens=[
    "<s>",
    "<pad>",
    "</s>",
    "<unk>",
    "<mask>",
])

save_path = './data'
tokenizer.save_tokenizer(save_path)

# tokenizer = ByteLevelBPETokenizer()

# vocab_size=5000
# # Customize training
# tokenizer.train(files=paths, vocab_size=vocab_size, min_frequency=5, special_tokens=[
#     "<s>",
#     "<pad>",
#     "</s>",
#     "<unk>",
#     "<mask>",
# ])

# save_path = 'tokenized_data'
# tokenizer.save_tokenizer(save_path)
