import os
from pathlib import Path
from tokenizers.models import BPE
from tokenizers import Tokenizer
from tokenizers.decoders import ByteLevel as ByteLevelDecoder
from tokenizers.normalizers import NFKC, Sequence
from tokenizers.pre_tokenizers import ByteLevel
from tokenizers.trainers import BpeTrainer
from tokenizers import ByteLevelBPETokenizer


class BPE_token(object):
    def __init__(self):
        self.tokenizer = Tokenizer(BPE())
        self.tokenizer.normalizer = Sequence([
            NFKC()
        ])
        self.tokenizer.pre_tokenizer = ByteLevel()
        self.tokenizer.decoder = ByteLevelDecoder()

    def bpe_train(self, paths):
        trainer = BpeTrainer(vocab_size=50000, show_progress=True, inital_alphabet=ByteLevel.alphabet(), special_tokens=[
            "<s>",
            "<pad>",
            "</s>",
            "<unk>",
            "<mask>"
        ])
        # self.tokenizer.train(trainer, paths)
        self.tokenizer.train(paths,trainer)

    def save_tokenizer(self, location, prefix=None):
        if not os.path.exists(location):
            os.makedirs(location)
        self.tokenizer.model.save(location, prefix)


# paths = [str(x) for x in Path('data').glob("**/train*.txt")]
# paths = ['data/train0.txt']
paths = ['dataorg/askans.txt']

tokenizer = BPE_token()
tokenizer.bpe_train(paths)
save_path = 'data/tokenizer_data'
tokenizer.save_tokenizer(save_path)


import json
config = {
  "model_type" : "roberta",
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.3,
  "hidden_size": 128,
  "initializer_range": 0.02,
  "num_attention_heads": 1,
  "num_hidden_layers": 1,
  "vocab_size": 50000,
  "intermediate_size": 256,
  "max_position_embeddings": 256
}
with open(save_path+'/config.json', 'w') as fp:
    json.dump(config, fp)
