
import os
from transformers import GPT2Config, TFGPT2LMHeadModel, GPT2Tokenizer, set_seed
import sqlite3
import wandb

os.system('wandb login 187758f8acd4c065f5f10a982e516222c1e743b1')

run = wandb.init(project="gptfimodel", entity='remotejob2')

runnum = "8"

# artifact = run.use_artifact('remotejob2/gptfimodel/gptfimodeltokenazer:v2', type='raw_data')
# artifact_dir = artifact.download()

# tokenizer_path = artifact_dir


# tokenizer = GPT2Tokenizer.from_pretrained(tokenizer_path)


artifact = run.use_artifact('remotejob2/gptfimodel/tweetGPTfimodel:v'+runnum, type='model')
artifact_dir = artifact.download() 
model_name_or_path = artifact_dir


tokenizer = GPT2Tokenizer.from_pretrained(model_name_or_path)

model = TFGPT2LMHeadModel.from_pretrained(model_name_or_path,from_pt=True)

ask = 'Varpaalla masturbointi'

input_ids = tokenizer.encode(ask.capitalize(), return_tensors='tf')
beam_output = model.generate(
    input_ids,
    max_length=100,
    # num_beams=3,
    # temperature=0.7,
    # top_p=0.92,
    top_k=50,
    top_p=0.95,
    # no_repeat_ngram_size=2,
    num_return_sequences=5,  # 75
    early_stopping=True,
    do_sample=True,
)

for phrase in beam_output:

    orgtxt = tokenizer.decode(phrase, skip_special_tokens=True)

    print(orgtxt)